# ParticleSwarmOptimization
a simple example of particle swarm optimization to solve for roots of an equation written in lua

simply enter an equation to find the roots. must be simple algebraic, not trig expression, nor complex values. 
user is also asked to input Min and Max values. these set the range for possible value for the root. 
  -if you enter 0 as min and 100 as max, it will only search values 0 to 100 as possible solution.
 user is also asked to set the number of individual particles. 

